#!/usr/bin/env python3
# CLI Control Center V0.4
import csv
import sys
import os
import datetime


def main():
    os.system("clear")
    menu()


# the menu
def menu():
    now = datetime.datetime.now()
    print(now.strftime("[%H:%M]"), 3 * "-", "Control Center V0.4-3", 10 * "-")
    choice = input("""
1: Keyboard setup       | 5: Cleaner                   | 9:  Apparmor
2: Install Nvidia       | 6: Updater                   | 10: Computer management
3: System info          | 7: Upgrade to newest release
4: GRUB config          | 8: Firewall

Please enter your choice (q to quit): """)

    if choice == "1":
        keyboard()
    elif choice == "2":
        nvidia_installer()
    elif choice == "3":
        sysinfo()
    elif choice == "4":
        grub()
    elif choice == "5":
        clean()
    elif choice == "6":
        updatesystem()
    elif choice == "7":
        release_upgrade()
    elif choice == "8":
        firewall()
    elif choice == "9":
        apparmor()
    elif choice == "10":
        com_mana()
    elif choice == "Q" or choice == "q":
        os.system("clear")
        sys.exit
    else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        menu()


# Finction to update the sustem
def updatesystem():
    os.system("sudo apt update")
    os.system("sudo apt upgrade")
    os.system("clear")
    menu()


# Function to clean the system
def clean():
    os.system("sudo apt clean")
    os.system("sudo apt autoremove")
    os.system("clear")
    menu()


# Finction to upgrade to new release
def release_upgrade():
    os.system("sudo do-release-upgrade")
    os.system("clear")
    menu()


# Finction to config and setup grub
def grub():
    os.system("clear")
    grub_input = input("""
----------------------------    
|1: Update GRUB            |
|2: Install/reinstall GRUB |
|3: Edit GRUB config file  |
----------------------------
Please enter your choice (q to go back): """)

    if grub_input == "1":
        update_grub()
        grub()
    elif grub_input == "2":
        install_grub()
        grub()
    elif grub_input == "3":
        edit_grub()
        grub()
    elif grub_input == "Q" or grub_input == "q":
        os.system("clear")
        menu()
    else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        grub()


# grub functions
def update_grub():
    os.system("sudo update-grub")


def edit_grub():
    os.system("sudo nano /etc/default/grub")


def install_grub():
    grub_input = input("Drive to install grub to? (/dev/sda) :")
    ins_grub = "sudo grub-install " + grub_input
    os.system(ins_grub)


# Finction to manage the apparmor
def apparmor():
    app_input = input("""
------------
|1: Status |
|2: Start  |
|3: Stop   |
------------

Please enter your choice (q to go back): """)

    if app_input == "1":
        os.system("sudo aa-status")
        apparmor()
    elif app_input == "2":
        os.system("sudo systemctl enable apparmor && sudo systemctl start apparmor")
        apparmor()
    elif app_input == "3":
        os.system("sudo systemctl stop apparmor && sudo systemctl disable apparmor")
        apparmor()
    elif app_input == "Q" or app_input == "q":
        os.system("clear")
        menu()
    else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        apparmor()


# Finction to manage the Firewall
def firewall():
    fw_input = input("""
------------
|1: Status |
|2: Start  |
|3: Stop   |
------------

Please enter your choice (q to go back): """)

    if fw_input == "1":
        os.system("sudo ufw status verbose")
        firewall()
    elif fw_input == "2":
        os.system("sudo ufw enable")
        os.system("sudo ufw status verbose")
        firewall()
    elif fw_input == "3":
        os.system("sudo ufw disable")
        os.system("sudo ufw status verbose")
        firewall()
    elif fw_input == "Q" or fw_input == "q":
        os.system("clear")
        menu()
    else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        firewall()


# Finction to set up the Keyboard
def keyboard():
    kb_input = input("""
1: Reconfig your keyboard

Please enter your choice (q to go back): """)

    if kb_input == "1":
        os.system("sudo dpkg-reconfigure keyboard-configuration")
        keyboard()
    elif kb_input == "Q" or kb_input == "q":
        os.system("clear")
        menu()
    else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        keyboard()


# Finction to install the nvidia driver
def nvidia_installer():
    ti_input = input("""
-----------------------------    
|1: Install Nvidia driver   |
|2: UnInstall Nvidia driver |
-----------------------------

Please enter your choice (q to go back): """)

    if ti_input == "1":
        os.system("sudo ubuntu-drivers autoinstall")
        os.system("clear")
        menu()
    elif ti_input == "2":
        os.system("sudo apt-get purge nvidia*")
        os.system("clear")
        menu()
    elif ti_input == "Q" or ti_input == "q":
        os.system("clear")
        menu()
    else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        menu()


def sysinfo():
    os.system("inxi -F")
    menu()


# computer management
def com_mana():
    com_input = input("""
--------------------------------------------------
|1: Disk space   | 4: List open files            |
|2: Ram use      | 5: Network packets statistics | 
|3: Disk manager | 6: Monitor network trafric    |
--------------------------------------------------
Please enter your choice (q to go back): """)

    if com_input == "1":
        os.system("df -h")
        com_mana()
    elif com_input == "2":
        os.system("free -h")
        com_mana()
    elif com_input == "3":
        os.system("sudo cfdisk")
        com_mana
    elif com_input == "4":
        os.system("sudo lsof | more")
        os.system("clear")
        com_mana()
    elif com_input == "5":
        os.system("netstat -a | more")
        os.system("clear")
        com_mana()
    elif com_input == "6":
        os.system("sudo nethogs")
        os.system("clear")
        com_mana()
    elif com_input == "Q" or com_input == "q":
        os.system("clear")
        menu()
    else:
        os.system("clear")
        print(29 * "-")
        print("|You must only select option|")
        print("|Please try again           |")
        print(29 * "-")
        com_mana()


# the program is initiated, so to speak, here
main()
